#include <Arduino.h>

void cpy_char (char a[], char b[])
{
  for(int i = 0; i < strlen(a); i++) {
    b[i] = a[i];
  }
}

void num_left_pad(char a[], int d)
{
  if (d > 99) {
    snprintf(a, 4, "%d", d);
  } else {
    snprintf(a, 4, " %d", d);
  }
}

void toggle_int (int *toggleVal) {
  if (*toggleVal == 1) {
    *toggleVal = 0;
  } else {
    *toggleVal = 1;
  }
}