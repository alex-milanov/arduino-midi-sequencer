#include "state.h"

// LiquidCrystal_I2C lcd;

void lcd_write_at (char c, int x, int y);

void lcd_prn (const char c[], int x, int y);

void lcd_init ();

void lcd_update (State state, State oldState);
