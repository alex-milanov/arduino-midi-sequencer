#include <Arduino.h>
#include <Wire.h>
#include <MIDI.h>
#include <LiquidCrystal_I2C.h>
#include "inc/helpers.h"
#include "inc/state.h"
#include "inc/lcd.h"
#include "inc/opl2.h"


#include <SoftwareSerial.h>
SoftwareSerial mySerial(2, 3); // RX, TX

// LiquidCrystal_I2C lcd(0x3f,16,2);          //    Задава се LCD адреса 0x27 , 16 символа ( знака ), 2 реда

// clock
// long bpm = 125;
long tempo = 1000/(125/60);
// int row = 0;
// int section = 2;

long prevmillis = 0;
long interval = tempo/24;


// Analog input
static const int PIN_TEMPO_POT = 1;
static const int PIN_BPM_POT = 0;
static const int UPBTNPIN = 2;
static const int PLAYBTNPIN = 3;
// char pattern[4][16] = {
//   {2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
//   {1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1},
//   {2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 2, 2, 1, 2, 1},
//   {1, 1, 1, 2, 2, 1, 1, 1, 2, 1, 1, 1, 2, 2, 1, 1}
// };
const int track_count = 4;
int inst_set[track_count] = {
  36,
  38,
  42,
  48
};

State state = {
  .section = 2,
  .track = 1,
  .bpm = 140,
  .patPos = 0,
  .beatStep = 0,
  .playing = 0,
  .recording = 0,
  .pattern = {
    {2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
    {1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1},
    {2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 2, 2, 1, 2, 1},
    // {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
    {1, 1, 1, 2, 2, 1, 1, 1, 2, 1, 1, 1, 2, 2, 1, 1}
  }
};
State oldState;

// int patIndex = 0;

// old values
uint32_t old_pot_val = 0;
uint32_t old_bpm_val = 0;

MIDI_CREATE_DEFAULT_INSTANCE();

void setup()
{
  // mySerial.begin(9600);
  // mySerial.println("Hello, world?");

  pinMode(UPBTNPIN, INPUT_PULLUP);
  pinMode(PLAYBTNPIN, INPUT_PULLUP);
  Serial.begin(115200);
  MIDI.begin(MIDI_CHANNEL_OMNI);
  lcd_init();                            //     Инициализация на LCD
  opl2_init();
}

uint8_t lastStepToggle = 1;
uint8_t lastPlayToggle = 1;

void loop()
{
  oldState = (State)state;
  uint32_t pot_val;
  uint32_t bpm_val;
  uint8_t stepToggle;
  uint8_t playToggle;
  char pattern_s[16];

  pot_val = analogRead(PIN_TEMPO_POT);
  bpm_val = analogRead(PIN_BPM_POT);
  state.section = int(((float(bpm_val) / 1023) * 12)) % 3;
  tempo = 1000/(state.bpm/60);
  interval = int(60000 / float(state.bpm));
  stepToggle = digitalRead(UPBTNPIN);
  playToggle = digitalRead(PLAYBTNPIN);

  // update state
  if (int(pot_val / 10) != old_pot_val || int(bpm_val / 10) != old_bpm_val) {
    switch (state.section) {
      // switch track
      case 0:
        if ((old_pot_val - 1) < int(pot_val / 10) < (old_pot_val + 1)) {
          state.track = int(((float(pot_val) / 1023) * 12)) % 4;
        }
        break;
      // modify bpm
      case 1:
        if (int(pot_val / 10) != old_pot_val) {
          state.bpm = int((float(pot_val) / 1023) * 280);
        }
        break;
      // move the position in the pattern
      case 2:
        state.patPos = 15 - int((pot_val) / 64);
        break;
    }
  }
  old_pot_val = int(pot_val / 10);
  old_bpm_val = int(bpm_val / 10);
  

  // toggle pattern position
  if (stepToggle < lastStepToggle) {
    state.pattern[state.track][state.patPos] = char(2 - (int(state.pattern[state.track][state.patPos]) - 1));
  }
  lastStepToggle = stepToggle;

  // toggle playing
  if (playToggle < lastPlayToggle) {
    toggle_int(&state.playing);
  }
  lastPlayToggle = playToggle;
  // todo handle clock reset on stop
  if (MIDI.read()) {
    switch (MIDI.getType()) {
      case midi::NoteOn:
        uint8_t chan;
        uint8_t note;
        // char note_s[3];
        chan = MIDI.getChannel();
        note = MIDI.getData1();
        // num_left_pad(note_s, note); 
        // lcd_prn(lcd, note_s, 10, 0);
        // control messages
        if (chan == 1) {
          // char track_s[3];
          switch (note) {
            // play/pause
            case 80:
              toggle_int(&state.playing);
              break;
            // stop
            case 81:
              state.playing = 0;
              state.recording = 0;
              state.beatStep = 0;
              break;
            // toggle recording
            case 67:
              toggle_int(&state.recording);
              break;
            // next track
            case 14:
              state.track = (track_count + (state.track + 1)) % track_count;
              // num_left_pad(track_s, state.track); 
              // lcd_prn(lcd, track_s, 1, 0);
              break;
            // prev track
            case 15:
              state.track = (track_count + (state.track - 1)) % track_count;
              // num_left_pad(track_s, state.track); 
              // lcd_prn(lcd, track_s, 1, 0);
              break;
          }
        } else if (chan == 10 && state.recording == 1) {
          for(int trck = 0; trck < track_count; trck++) {
            if (note == inst_set[trck]) {
              state.pattern[trck][state.beatStep] = char(2 - (int(state.pattern[trck][state.beatStep]) - 1));
            }
          }
        }
      break;
    }
  }

  // display
  lcd_update(state, oldState);

  // clock
  long currentMillis = millis();
  long timespan = currentMillis - prevmillis;
  long fl = interval / 4;
  int opl_kit_state[4] = {0, 0, 0, 0};

  if(timespan >= fl) {
    // send midi msgs
    if (state.playing == 1) {
      // MIDI.sendRealTime(MIDI_NAMESPACE::Clock);
      for(int inst = 0; inst < track_count; inst++) {
        MIDI.sendNoteOff(inst_set[inst], 0, 10);
        opl_kit_state[inst] = 0;
        if (state.pattern[inst][state.beatStep] == 2) {
          opl_kit_state[inst] = 1;
          MIDI.sendNoteOn(inst_set[inst], 127, 10);
        }
      }
      opl2_play(opl_kit_state);
    }

    if ((state.beatStep % 2) == 0) {
      cpy_char(state.pattern[state.track], pattern_s);
      pattern_s[state.patPos] += 2;
      // lcd_prn(pattern_s, 0, 1);
      for(int pos = 0; pos < 16; pos++) {
        if (
          state.beatStep == 0 || state.track != oldState.track
          || (state.patPos != oldState.patPos && (pos == state.patPos || pos == oldState.patPos))
          || (state.pattern[state.track][pos] != oldState.pattern[state.track][pos])) {
          lcd_write_at(pattern_s[pos], pos, 1);
          // lcd.setCursor(pos, 1);
          // lcd.write(pattern_s[pos]);
        }
      }
    }


    if (state.beatStep == 0 && state.section != 2) {
      // lcd.cursor_on();
    }

    if (state.beatStep < 15) {
      state.beatStep++;
    } else {
      state.beatStep = 0;
    }
    prevmillis += fl;
    delay(fl / 2);
    // lcd.cursor_off();
  } else {
    delay(2);
  }
  delay(interval / 32);
}
