#include "chars.h"
#include "helpers.h"
#include "state.h"
#include <LiquidCrystal_I2C.h>

const int lcd_width = 16;
const int lcd_height = 2;

LiquidCrystal_I2C lcd(0x3f, lcd_width, lcd_height);          //    Задава се LCD адреса 0x27 , 16 символа ( знака ), 2 реда

void lcd_write_at (char c, int x, int y) {
  lcd.setCursor(x, y);
  lcd.write(c);
}

void lcd_prn (const char c[], int x, int y)
{
  for(int i = 0; i < strlen(c) && (x + i) < lcd_width; i++) {
    lcd_write_at(c[i], x + i, y);
  }
}

void lcd_init () {
  lcd.init();                            //     Инициализация на LCD
  lcd.backlight();

  lcd.load_custom_character(1, char_box);
  lcd.load_custom_character(2, char_box_filled);
  lcd.load_custom_character(3, char_box_arrow);
  lcd.load_custom_character(4, char_box_filled_arrow);
  //
  lcd.load_custom_character(5, arr_up);
  lcd.load_custom_character(6, arr_down);
  lcd.load_custom_character(7, arr_up_down);

  delay(100);
  lcd.clear();                         //     Изстриване на екрана
  delay(100);
  lcd.setCursor(0, 0);
  lcd.write(7);
  // prn(lcd, " 0   140 4/4 ", 1, 0);        //     На дисплея се изписва текста ROBOTEV
  lcd_prn(" 0   140     ", 1, 0);        //     На дисплея се изписва текста ROBOTEV

  lcd.setCursor(2, 0);
  lcd.cursor_on();
}


void lcd_update (State state, State oldState) {
  // mySerial.print(state.section);
  // mySerial.print(" ");
  // mySerial.print(state.track);
  // mySerial.print(" ");
  // mySerial.print(state.bpm);
  // mySerial.print(" ");
  // mySerial.print(state.patPos);
  // mySerial.print(" ");
  // mySerial.print(state.beatStep);
  // mySerial.print(" ");
  // mySerial.println(state.pattern[0][0]);
  char pattern_s[16];
  char track_s[2];
  char bpm_s[3];
  // if the track has changed
  if (state.track != oldState.track) {
    num_left_pad(track_s, state.track); 
    lcd_prn(track_s, 1, 0);
  }
  // if the bpm has changed
  if (state.bpm != oldState.bpm) {
    num_left_pad(bpm_s, state.bpm); 
    lcd_prn(bpm_s, 6, 0);
  }

  // if (state.playing == 1 && state.beatStep == 0) {
  //   cpy_char(state.pattern[state.track], pattern_s);
  //   pattern_s[state.patPos] += 2;
  //   lcd_prn(pattern_s, 0, 1);
  // }
  // if the pattern has changed
  // cpy_char(state.pattern[state.track], pattern_s);
  // pattern_s[state.patPos] += 2;
  // for(int pos = 0; pos < lcd_width; pos++) {
  //   if ((state.track != oldState.track)
  //     || (state.patPos != oldState.patPos && (pos == state.patPos || pos == oldState.patPos))
  //     || (state.pattern[state.track][pos] != oldState.pattern[state.track][pos])) {
  //     lcd.setCursor(pos, 1);
  //     lcd.write(pattern_s[pos]);
  //   }
  // }
  // cursor set
  switch (state.section) {
    case 0:
      lcd.setCursor(2, 0);
      break;
    case 1:
      lcd.setCursor(8, 0);
      break;
    case 2:
      break;
    default:
      break;
  }
}

