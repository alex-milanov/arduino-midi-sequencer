#include <Arduino.h>

#ifndef IN_CHARS_H
#define IN_CHARS_H

byte char_box[8] = {
  B00000,
  B00000,
  B11111,
  B10001,
  B10001,
  B11111,
  B00000,
  B00000
};
byte char_box_filled[8] = {
  B00000,
  B00000,
  B11111,
  B11111,
  B11111,
  B11111,
  B00000,
  B00000
};
byte char_box_arrow[8] = {
  B00000,
  B00000,
  B11111,
  B10001,
  B10001,
  B11111,
  B00000,
  B11111
};
byte char_box_filled_arrow[8] = {
  B00000,
  B00000,
  B11111,
  B11111,
  B11111,
  B11111,
  B00000,
  B11111
};
byte arr_up[8] = {
  B00100,
  B01110,
  B11011,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000
};
byte arr_down[8] = {
  B00000,
  B00000,
  B00000,
  B00000,
  B11011,
  B01110,
  B00100,
  B00000
};
byte arr_up_down[8] = {
  B00100,
  B01110,
  B11011,
  B00000,
  B11011,
  B01110,
  B00100,
  B00000
};
byte mtbg_mt[8] = {
  B00100,
  B01110,
  B10101,
  B10101,
  B10101,
  B00100,
  B00100,
  B00000
};
byte mtbg_b[8] = {
  B00100,
  B11111,
  B10011,
  B10110,
  B10011,
  B11111,
  B00100,
  B00000
};
byte mtbg_g[8] = {
  B00100,
  B11111,
  B10011,
  B10000,
  B10110,
  B11110,
  B00100,
  B00000
};

#endif