#include <SPI.h>
#include <OPL2.h>
#include <instruments.h>

OPL2 opl2;

byte drums[4] = {DRUM_BASS, DRUM_SNARE, DRUM_HI_HAT, DRUM_TOM};

void opl2_init() {
	opl2.begin();

	// Set percussion mode and load instruments.
	Instrument bass = opl2.loadInstrument(INSTRUMENT_BDRUM2);
	Instrument snare = opl2.loadInstrument(INSTRUMENT_RKSNARE1);
	Instrument tom = opl2.loadInstrument(INSTRUMENT_TOM2);
	Instrument cymbal = opl2.loadInstrument(INSTRUMENT_CYMBAL1);
	Instrument hihat = opl2.loadInstrument(INSTRUMENT_HIHAT2);

	opl2.setPercussion(true);
	opl2.setDrumInstrument(bass, DRUM_BASS);
	opl2.setDrumInstrument(snare, DRUM_SNARE);
	opl2.setDrumInstrument(tom, DRUM_TOM);
	opl2.setDrumInstrument(cymbal, DRUM_CYMBAL);
	opl2.setDrumInstrument(hihat, DRUM_HI_HAT);
}

void opl2_play(int opl_kit_state[4]) {
    bool bass   = opl_kit_state[0] == 1; // drums[inst] == DRUM_BASS;           // Bass drum every 1st tick
	bool snare  = opl_kit_state[1] == 1;  //drums[inst] == DRUM_SNARE;          // Snare drum every 3rd tick
	bool tom    = opl_kit_state[3] == 1; //  == DRUM_TOM;            // Tom tom
	bool cymbal = 0; // drums[inst] == DRUM_CYMBAL;         // Cymbal every 32nd tick
	bool hiHat  = opl_kit_state[2] == 1;  //drums[inst] == DRUM_HI_HAT;         // Hi-hat every tick
    // opl2.playDrum(drums[inst], drum_octaves[inst], drum_notes[inst]);
    opl2.setDrums(bass, snare, tom, cymbal, hiHat);
}