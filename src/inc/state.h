#ifndef IN_STATE_H
#define IN_STATE_H

struct State {
  int section;
  int track;
  int bpm;
  int patPos;
  int beatStep;
  int playing;
  int recording;
  char pattern[4][16];
};

#endif